from django.conf.urls import url
from . import api

urlpatterns = [
    url(
        r'^rest/api/users/$',
        api.RestUserList.as_view(),
        name='rest_user_list_url'
    ),
    url(
        r'^rest/api/users/(?P<pk>\d+)/$',
        api.RestUserDetail.as_view(),
        name='rest_user_detail_url'
    ),
    url(
        r'^rest/api/tasks/$',
        api.RestTaskList.as_view(),
        name='rest_task_list_url'
    ),
    url(
        r'^rest/api/tasks/(?P<pk>[0-9]+)/$',
        api.RestTaskDetail.as_view(),
        name='rest_task_detail_url'
    )
]
