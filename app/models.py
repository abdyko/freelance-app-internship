from django.db import models
from django.contrib.auth.models import AbstractUser
from .errors import ExceedsLimit, InvalidAmount
from django.db import transaction
from django.core.validators import MinValueValidator
from decimal import Decimal


class User(AbstractUser):
    MAX_BALANCE = 10000
    MIN_BALANCE = 0

    MAX_DEPOSIT = 1000
    MIN_DEPOSIT = 1

    MAX_WITHDRAW = 1000
    MIN_WITHDRAW = 1

    freelancer = 'freelancer'
    consumer = 'consumer'

    STATUSES = (
        (freelancer, 'Freelancer'),
        (consumer, 'Сonsumer'),
    )

    balance = models.DecimalField(
        max_digits=7,
        decimal_places=2,
        default=0,
        validators=[MinValueValidator(Decimal('0.00'))],
    )
    status = models.CharField(max_length=50, choices=STATUSES, default=consumer)

    @classmethod
    def deposit(cls, amount, user_id):
        with transaction.atomic():
            account = cls.objects.get(pk=user_id)

            if not cls.MIN_DEPOSIT <= amount <= cls.MAX_DEPOSIT:
                raise InvalidAmount(amount)

            if account.balance + amount > cls.MAX_BALANCE:
                raise ExceedsLimit(account.balance, amount)

            account.balance += amount
            account.save()

    @classmethod
    def withdraw(cls, amount, user_id):
        with transaction.atomic():
            account = cls.objects.get(pk=user_id)

            if not cls.MIN_WITHDRAW <= amount <= cls.MAX_WITHDRAW:
                raise InvalidAmount(amount)

            if account.balance < amount:
                raise ExceedsLimit(account.balance, amount)

            account.balance -= amount
            account.save()

    def __str__(self):
        return '{} | {}'.format(self.first_name, self.status)


class Task(models.Model):
    is_open = 'open'
    in_process = 'in_process'
    is_closed = 'closed'

    STATUSES = (
        (is_open, 'Open'),
        (in_process, 'In process'),
        (is_closed, 'Closed'),
    )

    owner = models.ForeignKey(
        User,
        related_name='task_owner',
        on_delete=models.CASCADE,
    )
    executor = models.ForeignKey(
        User,
        related_name='task_executor',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=150)
    description = models.TextField(max_length=1500)
    price = models.DecimalField(
        max_digits=7,
        decimal_places=2,
        validators=[MinValueValidator(Decimal('0.00'))]
    )
    status = models.CharField(max_length=50, choices=STATUSES, default=is_open)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
