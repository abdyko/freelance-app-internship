from rest_framework import serializers
from .models import User, Task


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'status',
            'balance',
        )


class TaskSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(
        read_only=True
    )

    class Meta:
        model = Task
        fields = (
            'id',
            'owner',
            'executor',
            'title',
            'description',
            'price',
            'status',
        )

    def create(self, validated_data):
        validated_data['owner'] = self.context['request'].user
        return Task.objects.create(**validated_data)
