from .serializers import UserSerializer, TaskSerializer
from rest_framework import permissions
from .models import User, Task
from .permissions import IsUserOrReadOnly, IsAuthorOrReadOnly
from rest_framework import generics


class RestUserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class RestUserDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsUserOrReadOnly,
    )
    queryset = User.objects.all()
    serializer_class = UserSerializer


class RestTaskList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Task.objects.all()
    serializer_class = serializer = TaskSerializer


class RestTaskDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsAuthorOrReadOnly,
    )
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
