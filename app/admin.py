from django.contrib import admin
from app.models import User, Task

admin.site.register(User)
admin.site.register(Task)
