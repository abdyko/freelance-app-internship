from django.contrib.auth.hashers import make_password
from django.test import TestCase
from app.models import User
import mock


class UserModelTestCase(TestCase):
    def setUp(self):
        self.user = User(**{
            'username': 'test_username',
            'email': 'test_mail@test.com',
            'password': make_password('simple_password'),
            'first_name': 'the_test_firstname',
            'last_name': 'the_test_lastname',
            'balance': 1000,
            'status': 'freelancer'
        })
        self.user.save()

    def test_balance_deposit_method(self):
        deposit_amount = 500
        balance = self.user.balance
        self.user.deposit(deposit_amount, self.user.pk)
        user = User.objects.get(pk=self.user.id)
        new_balance = user.balance
        self.assertNotEqual(balance, new_balance)
        self.assertEqual(balance + deposit_amount, new_balance)

    def test_balance_withdraw_method(self):
        withdraw_amount = 500
        balance = self.user.balance
        self.user.withdraw(withdraw_amount, self.user.pk)
        user = User.objects.get(pk=self.user.pk)
        new_balance = user.balance
        self.assertNotEqual(balance, new_balance)
        self.assertEqual(balance - withdraw_amount, new_balance)

    def test_amount_of_withdraw_less_than_min_withdraw(self):
        value = self.user.MIN_WITHDRAW - 1
        with self.assertRaises(Exception) as context:
            self.user.withdraw(0, self.user.pk)
        self.assertTrue(
            'Invalid Amount {}'.format(value) in str(context.exception)
        )

    def test_amount_of_deposit_less_than_min_deposit(self):
        value = self.user.MIN_DEPOSIT - 1
        with self.assertRaises(Exception) as context:
            self.user.deposit(value, self.user.pk)
        self.assertTrue(
            'Invalid Amount {}'.format(value) in str(context.exception)
        )

    @mock.patch('app.models.User.MAX_BALANCE', 1000)
    def balance_and_deposit_amount_is_greater_than_max_balance(self):
        deposit_amount = self.user.MAX_DEPOSIT
        with self.assertRaises(Exception) as context:
            self.user.deposit(deposit_amount, self.user.pk)
        exception_msg = 'amount: {}, current balance: {}'.format(
                deposit_amount, self.user.balance
        )
        self.assertTrue(exception_msg in str(context.exception))

    @mock.patch('app.models.User.MAX_WITHDRAW', 10000)
    def balance_and_withdraw_amount_is_less_than_the_min_withdraw(self):
        withdraw_amount = self.user.MAX_WITHDRAW
        with self.assertRaises(Exception) as context:
            self.user.withdraw(withdraw_amount, self.user.pk)
        exception_msg = 'amount: {}, current balance: {}'.format(
                withdraw_amount, self.user.balance
        )
        self.assertTrue(exception_msg in str(context.exception))
