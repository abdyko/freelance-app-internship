from django.test import TestCase
from app.models import User, Task
from django.contrib.auth.hashers import make_password
from app.serializers import UserSerializer, TaskSerializer


class UserSerializerTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(**{
            'username': 'test_username',
            'email': 'test_mail@test.com',
            'password': make_password('simple_password'),
            'first_name': 'the_test_firstname',
            'last_name': 'the_test_lastname',
            'balance': 1000,
            'status': 'freelancer'
        })
        cls.serializer = UserSerializer(instance=cls.user)

    def test_user_expected_fields(self):
        data = self.serializer.data
        self.assertEqual(
            set(data.keys()),
            set([
                'id',
                'username',
                'first_name',
                'last_name',
                'email',
                'status',
                'balance'
            ])
        )


class TaskSerializerTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(**{
            'username': 'test_username',
            'email': 'test_mail@test.com',
            'password': make_password('simple_password'),
            'first_name': 'the_test_firstname',
            'last_name': 'the_test_lastname',
            'balance': 1000,
            'status': 'freelancer'
        })
        cls.user.save()

        cls.task = Task(**{
            'owner': cls.user,
            'title': 'created task',
            'description': 'asd\r\nasd\r\na\r\nsd\r\nad',
            'price': '300.00',
            'status': 'open'
        })
        cls.serializer = TaskSerializer(instance=cls.task)

    def test_task_expected_fields(self):
        data = self.serializer.data
        self.assertEqual(
            set(data.keys()),
            set(['id', 'owner', 'title', 'price', 'executor', 'status', 'description'])
        )
