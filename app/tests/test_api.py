from django.contrib.auth.hashers import make_password
from django.test import TestCase
from app.models import User, Task
from django.urls import reverse
from app.serializers import UserSerializer, TaskSerializer
import simplejson as json

default_user_data = {
    'username': 'test_user',
    'email': 'test_mail@mail.py',
    'password': make_password('simple_password'),
    'first_name': 'the_test_firstname',
    'last_name': 'the_test_lastname',
    'balance': 300,
}


def create_user_data(**kwargs):
    data = kwargs['default_data']
    new_data = {
        'username': kwargs['username'],
        'email': data['email'],
        'password': data['password'],
        'first_name': data['first_name'],
        'last_name': data['last_name'],
        'status': kwargs['status'],
    }
    return new_data


def create_task_data(owner, title='test_title', price=300):
    new_data = {
        'owner': owner,
        'title': title,
        'description': 'the task test description',
        'price': price
    }
    return new_data


class UsersGetPostTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_consumer = User(**create_user_data(
            username='test_consumer',
            status='consumer',
            default_data=default_user_data
        ))
        cls.user_consumer.save()
        cls.user_freelancer = User(**create_user_data(
            username='for_delete',
            status='freelancer',
            default_data=default_user_data
        ))
        cls.user_freelancer.save()

    def test_get_users_list(self):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        response = self.client.get(
            reverse(
                'rest_user_list_url'
            )
        )
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, 200)

    def test_create_user(self):
        response = self.client.post(
            reverse('rest_user_list_url'),
            data=json.dumps(create_user_data(
                username='test_consumer_1',
                status='freelancer',
                default_data=default_user_data
                )
            ),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)


class UsersGetPutDeleteTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_consumer = User(**create_user_data(
            username='test_consumer',
            status='consumer',
            default_data=default_user_data
        ))
        cls.user_consumer.save()

    def test_get_user_detail(self):
        response = self.client.get(
            reverse(
                'rest_user_detail_url',
                kwargs={'pk': self.user_consumer.id}
            )
        )
        serializer = UserSerializer(self.user_consumer)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, 200)

    def test_put_update_user_profile_if_is_owner(self):
        self.client.login(
            username=self.user_consumer.username,
            password='simple_password'
        )
        response = self.client.put(
            reverse(
                'rest_user_detail_url',
                kwargs={'pk': self.user_consumer.id}
            ),
            data=json.dumps(create_user_data(
                username='test_consumer_1',
                status='freelancer',
                default_data=default_user_data
            )),
            content_type='application/json'
        )
        user = User.objects.get(id=self.user_consumer.id)
        old_data = self.user_consumer.status
        new_data = user.status
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(new_data, old_data)

    def test_put_update_user_profile_if_is_not_owner(self):
        response = self.client.put(
            reverse(
                'rest_user_detail_url',
                kwargs={'pk': self.user_consumer.id}
            ),
            data=json.dumps(create_user_data(
                username='test_consumer_1',
                status='freelancer',
                default_data=default_user_data
            )),
            content_type='application/json'
        )
        user = User.objects.get(id=self.user_consumer.id)
        old_data = self.user_consumer.status
        new_data = user.status
        self.assertEqual(response.status_code, 403)
        self.assertEqual(new_data, old_data)

    def test_delete_user(self):
        self.client.login(
            username=self.user_consumer.username,
            password='simple_password'
        )
        response = self.client.delete(
            reverse(
                'rest_user_detail_url',
                kwargs={'pk': self.user_consumer.id}
            )
        )
        self.assertEqual(response.status_code, 204)


class TasksGetPostTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_consumer = User(**create_user_data(
            username='test_consumer',
            status='consumer',
            default_data=default_user_data
        ))
        cls.user_consumer.save()

        cls.task = Task(**{
                "owner": cls.user_consumer,
                "title": "created task",
                "description": "asd\r\nasd\r\na\r\nsd\r\nad",
                "price": "300.00",
                "status": "open",
            }
        )
        cls.task.save()

    def test_get_task_list(self):
        tasks = Task.objects.all()
        serializer = TaskSerializer(tasks, many=True)
        response = self.client.get(
            reverse(
                'rest_task_list_url'
            )
        )
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, 200)

    def test_post_create_task(self):
        self.client.login(
            username=self.user_consumer.username,
            password='simple_password'
        )
        self.assertIn('_auth_user_id', self.client.session)
        response = self.client.post(
            reverse('rest_task_list_url'),
            data=json.dumps({
                "title": "created task",
                "description": "asd\r\nasd\r\na\r\nsd\r\nad",
                "price": "300.00",
                "status": "open",
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)


class TasksGetPutDeleteTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_consumer = User(**create_user_data(
            username='test_consumer',
            status='consumer',
            default_data=default_user_data
        ))
        cls.user_consumer.save()

        cls.user_freelancer = User(**create_user_data(
            username='test_freelancer',
            status='freelancer',
            default_data=default_user_data
        ))
        cls.user_consumer.save()

        cls.task = Task(**create_task_data(
            owner=cls.user_consumer,
            title='test_task_title',
        ))
        cls.task.save()

    def test_get_task_detail(self):
        self.client.login(
            username=self.user_consumer.username,
            password='simple_password'
        )
        response = self.client.get(
            reverse(
                'rest_task_detail_url',
                kwargs={'pk': self.task.id}
            )
        )
        serializer = TaskSerializer(self.task)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, 200)

    def test_put_update_task_not_from_owner(self):
        self.client.login(
            username=self.user_freelancer.username,
            password='simple_password'
        )
        response = self.client.put(
            reverse(
                'rest_task_detail_url',
                kwargs={'pk': self.task.id}
            ),
            data=json.dumps({
                "title": "created task",
                "description": "asd\r\nasd\r\na\r\nsd\r\nad",
                "price": "300.00",
                "status": "open",
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 403)

    def test_put_update_task_from_owner(self):
        self.client.login(
            username=self.user_consumer.username,
            password='simple_password'
        )
        response = self.client.put(
            reverse(
                'rest_task_detail_url',
                kwargs={'pk': self.task.id}
            ),
            data=json.dumps({
                "title": "created task",
                "description": "asd\r\nasd\r\na\r\nsd\r\nad",
                "price": "300.00",
                "status": "open",
            }),
            content_type='application/json'
        )
        task = Task.objects.get(id=self.task.id)
        old_data = self.task.title
        new_data = task.title
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(new_data, old_data)

    def test_delete_task(self):
        self.client.login(
            username=self.user_consumer.username,
            password='simple_password'
        )
        response = self.client.delete(
            reverse(
                'rest_task_detail_url',
                kwargs={'pk': self.task.id}
            )
        )
        self.assertEqual(response.status_code, 204)
