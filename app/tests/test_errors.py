from django.test import TestCase
from app.errors import ExceedsLimit, InvalidAmount


def raiseExceedsLimit(balance, amount):
    raise ExceedsLimit(balance, amount)


def raiseInvalidAmount(amount):
    raise InvalidAmount(amount)


class ErrorsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.balance = 1000
        cls.amount = 500

    def testExceedsLimit(self):
        with self.assertRaises(Exception) as context:
            raiseExceedsLimit(self.balance, self.amount)
        exception_msg = 'amount: {}, current balance: {}'.format(
                self.amount, self.balance
        )
        self.assertTrue(exception_msg in str(context.exception))

    def testInvalidAmount(self):
        with self.assertRaises(Exception) as context:
            raiseInvalidAmount(self.amount)
        self.assertTrue(
            'Invalid Amount {}'.format(self.amount) in str(context.exception)
        )
