from mock import MagicMock
from django.test import TestCase
from app.models import User
from django.contrib.auth.hashers import make_password
from app.permissions import IsUserOrReadOnly, IsAuthorOrReadOnly


class PermissionsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.IsAuthorOrReadOnly = IsAuthorOrReadOnly()
        cls.IsUserOrReadOnly = IsUserOrReadOnly()

        cls.user_consumer = User(**{
            'username': 'test_username',
            'email': 'test_mail@test.com',
            'password': make_password('simple_password'),
            'first_name': 'the_test_firstname',
            'last_name': 'the_test_lastname',
            'balance': 1000,
            'status': 'freelancer'
        })
        cls.user_consumer.save()

        cls.user_freelancer = User(**{
             'username': 'test_freelancerasdas',
             'email': 'test2_mail@test.com',
             'password': make_password('simple_password'),
             'first_name': 'freelancer',
             'last_name': 'freelancer',
             'balance': 1000,
             'status': 'freelancer'
        })
        cls.user_freelancer.save()
        cls.view = MagicMock()

    def test_IsAuthorOrReadOnly_returns_true_when_user_is_task_owner(self):
        task = MagicMock(owner=self.user_consumer)
        request = MagicMock(user=self.user_consumer, method='POST')
        self.assertTrue(self.IsAuthorOrReadOnly.has_object_permission(
                request, self.view, task
            )
        )

    def test_IsAuthorOrReadOnly_returns_false_when_user_isnt_task_owner(self):
        task = MagicMock(owner=self.user_freelancer)
        request = MagicMock(user=self.user_consumer, method='POST')
        self.assertFalse(self.IsAuthorOrReadOnly.has_object_permission(
                request, self.view, task
            )
        )

    def test_IsAuthorOrReadOnly_returns_true_when_request_method_get(self):
        task = MagicMock(owner=self.user_freelancer)
        request = MagicMock(method='GET')
        self.assertTrue(self.IsAuthorOrReadOnly.has_object_permission(
                request, self.view, task
            )
        )

    def test_IsUserOrReadOnly_returns_true_when_user_is_account_owner(self):
        task = MagicMock(id=self.user_consumer.id)
        request = MagicMock(user=self.user_consumer, method='POST')
        self.assertTrue(self.IsUserOrReadOnly.has_object_permission(
                request, self.view, task
            )
        )

    def test_IsUserOrReadOnly_returns_false_when_user_isnt_account_owner(self):
        task = MagicMock(id=self.user_freelancer.id)
        request = MagicMock(user=self.user_consumer, method='POST')
        self.assertFalse(self.IsUserOrReadOnly.has_object_permission(
                request, self.view, task
            )
        )

    def test_IsUserOrReadOnly_returns_true_when_request_method_get(self):
        task = MagicMock(owner=self.user_freelancer)
        request = MagicMock(method='GET')
        self.assertTrue(self.IsUserOrReadOnly.has_object_permission(
                request, self.view, task
            )
        )
