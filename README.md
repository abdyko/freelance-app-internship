[![coverage report](https://gitlab.com/abdyko/freelance-app-internship/badges/create_ci/coverage.svg)](https://gitlab.com/abdyko/freelance-app-internship/commits/create_ci)
[![pipeline status](https://gitlab.com/abdyko/freelance-app-internship/badges/create_ci/pipeline.svg)](https://gitlab.com/abdyko/freelance-app-internship/commits/create_ci)

#### freelance-app-internship

Server application freelance exchange. The application is built on the
django framework and written in python. The application has a customer,
executor, the ability to create jobs.


#### production requirements

Django==2.1.4
djangorestframework==3.9.0
pbr==5.1.1
python-dotenv==0.10.1
pytz==2018.9
six==1.12.0


#### tests requirements

coverage==3.7
simplejson==3.16.0
mock==2.0.0


#### Install

Server side instalation:
```
# Project clone
git clone

# create virtual env
python -m venv venv

# activate virtual env
source venv/bin/activate

# Install dependencies
pip install -r requirements.txt -r tests_require.txt

# Create env
change .env.example to .env and change all variables into this file

# Create database, create user, start server
./manage.py migrate
./manage.py createsuperuser
./manage.py runserver 127.0.0.1:8000

# API URLS
tasks
http://localhost:8000/rest/api/tasks/
task detail
http://localhost:8000/rest/api/tasks/#id

users
http://localhost:8000/rest/api/users/
user detail
http://localhost:8000/rest/api/users/#id
```
